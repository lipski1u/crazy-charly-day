<?php

namespace app\vue;

use app\model\Item as Item;
use app\model\Piece as Piece;
use app\model\Type as Type;
use app\model\Commentaire as Commentaire;
use app\model\Aimer as Aimer;


class VueCatalogue{

private $tabItem;



public function __construct($tItem){
$this->tabItem = $tItem;
}


public function render( $choixAffichage ){
session_start();
if(!isset($_SESSION['utilisateur'])) {
    $_SESSION['utilisateur'] = null;
};
define('PREFIX_SALT', 'choca');
define('SUFFIX_SALT', 'pic');   

echo '<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Crazy Charlie Day 2015</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
<!--script src="js/less-1.3.3.min.js"></script-->
<!--append ‘#!watch’ to the browser URL, then refresh the page. -->

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<![endif]-->

<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="img/favicon.png">

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
</head>

<body>';

include 'includes/headerCata.php';
//FIN DU HEADER DE LA PAGE

//CHOIX DU CONTENU DE LA PAGE
switch ($choixAffichage) {
case 0:
break;
case 1:
$this->listerItem();
break;
case 2:
$this->afficherItem();
break;
case 3:
echo $this->listerPiece();
break;
case 4:
echo $this->listerType();
break;
case 5:
echo $this->listerCouleur();
break;
case 6:
case 7:
$this->connexion_inscription();
break;
case 8:
$this->deconnexion();
break;

default:
# code...
break;
}

//FERMETURE DU CONTAINER ET DE LA PAGE
echo '</div></body><html>';

}


//AFFICHER LISTE DES BILLETS
private function listerItem(){


foreach ($this->tabItem as $item) {

//description piece_id photo, couleur, prix, type_id

$nom = $item->nom;
$photo = $item->photo; 
$description = $item->description;
$prix = $item->prix;

$type = Type::find($item->type_id);
$piece = Piece::find($item->piece_id);

echo 
'<div class="row clearfix">

<div class="col-md-2 column">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<p class="categorie">Categorie :</br>'.$piece->nom.'</p>
			<p class="piece">Piece :</br><a href="type?id='.$type->id.'" > '.$type->type.'</a></p>
		</div>


	</div>
</div>
<div class="col-md-2 column">
	<h2 class="nom"><a href="item?id='.$item->id.'" >'.$nom.'</a></h2>
</div>
<div class="col-md-3 column">
	<p><img src="image/'.$photo.'" height="200" width="200"></p>
</div>
<div class="col-md-3 column">
	<p class="description">description :'.$description.'</p>
</div>
<div class="col-md-1 column">
	<p class="prix">'.$prix.' €</p>
</div>
<div class="col-md-1 column">
	<p class="like">+</p>
</div>
</div>';

echo '<div class="col-xs-12"><hr></div>';
}

}

//AFFICHAGE BILLET UNIQUE
private function afficherItem(){

echo '<div class="row clearfix">';

$item=$this->tabItem;

//description piece_id photo, couleur, prix, type_id

$nom = $item->nom;
$photo = $item->photo; 
$description = $item->description;
$prix = $item->prix;
$note = 0;
$like = 0;

$tCom = Commentaire::where('id_item', '=', $item->id)->get();

foreach ($tCom as $com ) {
	$note = $note + $com->note;
}

$like = Aimer::where('id_item', '=', $item->id)->count();

echo'
<div class="row clearfix">

<div class="col-md-6 column">
	<div class="row clearfix">
		<h2 class="nom">'.$nom.'</h2>
	</div>

	<div class="row clearfix">
		<p class="image"><img src="image/'.$photo.'" height="200" width="200"></img></p>
	</div>
	<div class="row clearfix">
		<div class="col-md-3 column">
			<p class="note">Note : '.$note.'</p>
		</div>

		<div class="col-md-3 column">
			<p class="like">Like : '.$like.'</p>
		</div>

		<div class="col-md-6 column">
		</div>


	</div>
	
</div>

<div class="col-md-6 column">
	<div class="row clearfix">
		<div class="row clearfix">
			<p class="description">Description'.$description.'</p>
		</div>

		<div class="row clearfix">
			<div class="col-md-8 column">
			</div>

			<div class="col-md-4 column">
				<p class="prix">'.$prix.' €</p>
			</div>
		
		</div>
	</div>

	<div class="row clearfix">';
		echo "<form name=\"inscription\" method=\"post\" action=\"item?id=".$item->id."\">
		Entrez votre commentaire : <input type=\"text\" name=\"com\"/> <br/>
        Ajouter une note : <tr align=\"center\">
        <td><select name=\"Note\">";
            for ($i=1; $i < 6; $i++){ 
            	echo "<option value=\"".$i."\">".$i."</option>";

            };
        echo '</select></td><br>

        <input type="hidden" name="item" value='.$item->id.'/>
        <input type="hidden" name="user" value='.$_SESSION['utilisateur'].'/>
        <input type="submit" name="valider" value="OK"/>		
		</form>';

	foreach ($tCom as $com) {
		echo '	
		<div class="col-md-2 column"><p>'
		.$com->id_internaute.
		'</p></div>
		<div class="col-md-8 column"><p>'
		.$com->body.
		'</p></div>
		<div class="col-md-2 column"><p>'
		.$com->id_internaute.
		'</p></div>';
	}

	echo '</div>
</div>
</div>';


echo '</div>';
}



public function listerPiece() {

	$var ="" ;
	foreach ($this->tabItem as $key => $value){
		$var = $var + "<div class='col-md-4 column'>
			<h2>
				"+$value->nom+"
			</h2>
			<p>
				"+$value->desription+"
			</p>
			<p>
				<a class='btn' href='catalogue/listepiece?id="+$key+"'>Afficher les détails</a>
			</p>
		</div>";
	}
	return $var;
}
	
	
	
public function listerType() {
		
		$var ="" ;
		foreach ($this->tabItem as $key => $value){
			$var = $var +"<div class='col-md-4 column'>
			<h2>
				"+$value->type+"
			</h2>
			<p>
				<a class='btn' href='catalogue/listetype?id="+$key+"'>Afficher les détails</a>
			</p>
		</div>";
		}
		return $var;
}

public function listerCouleur() {
		
		$var = "";
		foreach ($this->tabItem as $key => $value){
			$var = $var + "<div class='col-md-4 column'>
			<h2>
				"+$key+"
			</h2>
			<p>
				<a class='btn' href='catalogue/listecouleur?couleur="+$key+"'>Afficher les détails</a>
			</p>
		</div>";
		}
		return $var;
}

public function connexion_inscription() {

		echo '<div class="row clearfix">

		<div class="col-md-12 column">

			<div class="col-md-6 column">
				<h3> Connexion </h3>
        		<div class="col-xs-12"><hr></div>

        		<form name="connexion" method="post" action="connexion">

        		<div class="row clearfix">
        			<div class="col-md-6 column"> Pseudo : </div>
        			<div class="col-md-6 column"><input type="text" name="pseudo"/></div>
        		</div>
        		
        		<div class="row clearfix">
        			<div class="col-md-6 column"> Mot passe : </div>
        			<div class="col-md-6 column"><input type="password" name="pass"/></div>
        		</div>

        		<div class="row clearfix">
        			<div class="col-md-4 column"></div>
        			<div class="col-md-4 column"><input type="submit" name="valider8" value="OK"/></div>
        			<div class="col-md-4 column"></div>
        		</div>

        		</form>
        	</div>

        	<div class="col-md-6 column">
        		<h3> Inscription </h3>
        		<div class="col-xs-12"><hr></div>

        		<form name="inscription" method="post" action="inscription">

        		<div class="row clearfix">
        			<div class="col-md-6 column">Pseudo : </div>
        			<div class="col-md-6 column"><input type="text" name="pseudo"/></div>
        		</div>

        		<div class="row clearfix">
        			<div class="col-md-6 column">Mot passe : </div>
        			<div class="col-md-6 column"><input type="password" name="pass"/></div>
        		
        		<div class="row clearfix">
        			<div class="col-md-4 column"></div>
        			<div class="col-md-4 column"><input type="submit" name="valider7" value="OK"/></div>
        			<div class="col-md-4 column"></div>
        		</div>

        		</form>
        	</div>
       	</div>
       	</div>';
     
		
    }

public function deconnexion() {
    session_unset();
    header("Refresh: 0;acceuil");     
}

}
?>