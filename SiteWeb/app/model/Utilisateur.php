<?php

namespace blogapp\model;

class Utilisateur extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'ccd_utilisateur';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
}