<?php

namespace app\model;

class Item extends \Illuminate\Database\Eloquent\Model{

	protected $table = 'ccd_items';
	protected $primaryKey = 'id' ;
	public $timestamps = false;

	public function getType() {
		$temp = $this->belongsTo('app\model\Type','id')->get();
		return $temp[0];
	}

	public function getPiece() {
		$temp = $this->belongsTo('app\model\Piece','id')->get();
		return $temp[0];		
	}
	
	public function getCommentaire(){
		$temp = $this->hasMany('app\model\Commentaire','id')->get();
		return $temp[0];
	}
	
	public function getAimer(){
		$temp = $this->hasMany('app\model\Aimer','id')->get();
		return $temp[0];
	}
}


?>