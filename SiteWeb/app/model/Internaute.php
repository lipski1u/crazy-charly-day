<?php

namespace app\model;

class Internaute extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'ccd_internaute';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
	public function getCommentaire() {
		return $this->hasMany('app\model\Commentaire','id')->get();
	}

	public function getAimer() {
		return $this->hasMany('app\model\Aimer','id')->get();
	}

}

?>