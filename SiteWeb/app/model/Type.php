<?php

namespace app\model;

class Type extends \Illuminate\Database\Eloquent\Model{

	protected $table = 'ccd_types';
	protected $primaryKey = 'id' ;
	public $timestamps = false;

	public function getItems() {
		return $this->hasMany('app\model\Item','id')->get();
	}
}


?>