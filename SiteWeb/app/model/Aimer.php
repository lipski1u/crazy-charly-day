<?php

namespace app\model;

class Aimer extends \Illuminate\Database\Eloquent\Model{

	protected $table = 'ccd_aimer';
	protected $primaryKey = 'id' ;
	public $timestamps = false;

	public function getInternaute() {
		return $this->belongsTo('app\model\Internaute','id')->get();
	}

	public function getItem() {
		return $this->belongsTo('app\model\Item','id')->get();
	}
}


?>