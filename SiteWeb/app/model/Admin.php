<?php

namespace app\model;

class Admin extends \Illuminate\Database\Eloquent\Model{

	protected $table = 'ccd_selection_admin';
	protected $primaryKey = 'id' ;
	public $timestamps = false;

	public function getPieces() {
		return $this->hasMany('app\model\Piece','adm_id')->get();
	}

	public function getTypes() {
		return $this->hasMany('app\model\Type','adm_id')->get();
	}

	public function getItem() {
		return $this->hasMany('app\model\Item','adm_id')->get();
	}
}


?>