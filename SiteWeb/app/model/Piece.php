<?php

namespace app\model;

class Piece extends \Illuminate\Database\Eloquent\Model{

	protected $table = 'ccd_pieces';
	protected $primaryKey = 'id' ;
	public $timestamps = false;

	public function getItems() {
		return $this->hasMany('app\model\Item','id')->get();
	}
}


?>