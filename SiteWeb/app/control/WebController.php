<?php

namespace app\control;

use \app\control\AbstractController;
use \app\model\Item;
use \app\model\Piece;
use \app\model\Type;
use \app\vue\VueCatalogue;
use \app\model\Internaute;
use \app\model\Commentaire;

class WebController extends AbstractController{	
		
	//fonction afficher acceuil
	public function acceuil(){
		$vue=new  VueCatalogue(Item::all());
		$vue->render(0);
	}

	//fonction afficher liste item
	public function affListItem(){
		$it=Item::all();
		$vue=new  VueCatalogue($it);
		$vue->render(1);
	}

	//fonction afficher item
	public function affItem(){
		$id=$this->request->get['id'];
		$it=Item::find($id);
		$vue=new  VueCatalogue($it);
		$vue->render(2);
				if(isset($_POST['valider'])) {
				$commentaire = new Commentaire;
				$commentaire->body = $_POST['com'];
				$commentaire->id_item =$_POST['item'];
				$commentaire->id_internaute=$_POST['user'];
				$commentaire->note = $_POST['Note'];
				$commentaire->save();
			}
	}

	//fonction afficher liste piece
	public function affListPiece(){
		$p=Piece::all();
		$vue=new  VueCatalogue($p);
		$vue->render(3);
	}

	//fonction afficher liste item par piece
	public function affListItemPiece(){
		$id=$this->request->get['id'];
		$p=Piece::find($id);
		$vue=new  VueCatalogue($p->getItems());
		$vue->render(1);
	}

	//fonction afficher liste type
	public function affListType(){
		$t=Type::all();
		$vue=new  VueCatalogue($t);
		$vue->render(4);
	}

	//fonction afficher liste item par type
	public function affListItemType(){
		$id=$this->request->get['id'];
		$t=Type::find($id);
		$vue=new  VueCatalogue($t->getItems());
		$vue->render(1);
	}

	//fonction afficher liste couleur
	public function affListCouleur(){
		$item=Item::all();
		$c=array();
		foreach($item as $item){
			$c[$item->couleur]=0;
		}
		$vue=new  VueCatalogue($c);
		$vue->render(5);
	}

	//fonction afficher liste item par couleur
	public function affListItemCouleur(){
		$couleur=$this->request->get['couleur'];
		$c=Item::where('couleur','LIKE',$couleur)->get();
		$vue=new  VueCatalogue($c);
		$vue->render(1);
	}


	public function connexion(){
		$vue=new VueCatalogue(null);
		$vue->render(6);
		if(isset($_POST['valider8'])) {
                $pseudo=$_POST['pseudo'];
                $pass=$_POST['pass'];
                $passHash=sha1(PREFIX_SALT.$pass.SUFFIX_SALT);
                $allUser=Internaute::all();
                $ii=0;
                $trouve=false;
                foreach ($allUser as $key => $value) {
                    $pseudoUti= $allUser[$ii]->pseudo;
                    $mdpUti = $allUser[$ii]->pass;
                    if (($pseudoUti==$pseudo) && ($mdpUti==$passHash)) {
                        $trouve=true;
                        $id=$allUser[$ii]->id;
                        $droit=$allUser[$ii]->droits;
                    }
                    $ii=$ii+1;
                };
                        if ($trouve == true) {
                                $_SESSION['utilisateur'] = $id;
                                $_SESSION['droit'] = $droit ;
                                header("Refresh: 0;acceuil");
                        }else{
                            echo "<p>Erreur : identifiant inconnu</p>";
                        }          
            }
      	if(isset($_POST['valider7'])) {
            $pseudo=$_POST['pseudo'];
            $pass=$_POST['pass'];
            $passHash=sha1(PREFIX_SALT.$pass.SUFFIX_SALT);
            $newUser = new Internaute;
            $newUser->pseudo = $pseudo;
           	$newUser->pass = $passHash;
            $newUser->droits=1;
            $newUser->save();              
        }
	}

	public function deconnexion(){
		$vue=new  VueCatalogue(null);
		$vue->render(7);
	}
}
?>