<?php

namespace conf;

use Illuminate\Database\Capsule\Manager as DB;

class Connection {

	public static function connection($init){
		$infocon=parse_ini_file($init);
		$db = new DB();
		$db->addConnection(array (
		'driver' => $infocon["driver"],
		'host' => $infocon["host"],
		'database' => $infocon["database"],
		'username' => $infocon["username"],	
		'password' => $infocon["password"],
		'charset' => 'utf8',
		'collation' => 'utf8_unicode_ci',
		'prefix' => ''
		));
	
		$db->setAsGlobal();
		$db->bootEloquent();
	}
}