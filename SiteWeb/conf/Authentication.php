<?php

namespace conf;
use \blogapp\model\User;

class Authentication {

	public static function createUser ( $userName, $password ,$prenom, $nom, $email) {
		// vérifier la conformité de $password avec la police
		
		// si ok : hacher $password
		$password=crypt($password,'$2a$05$jemetdurandom');
		// créer et enregistrer l'utilisateur
		$user=User::find($userName);
		if(!isset($user)){
		$newuser= new User();
		$newuser->prenom=$prenom;
		$newuser->mdp=$password;
		$newuser->nom=$nom;
		$newuser->login=$userName;
		$newuser->email=$email;
		$newuser->droits=1;
		$newuser->save();
		}else{
			throw new \Exception();
		}
	}

	public static function authenticate ( $username, $password ){
		// charger utilisateur $user
		$user_all=User::all();
		$user_account=null;
		foreach($user_all as $user){
			if(strcmp($user->name,$username)){
				$user_account=$user;
			}
		}
		// vérifier $user->hash == hash($password)
		if(!empty($user->mdp)){
			$password=crypt($password,'$2a$05$jemetdurandom');
			if($password==$user->mdp){
				// charger profil ($user->id)
				$_SESSION['id']=$user->id;
				Authentication::loadProfile( $_SESSION['id'] );
			}
		}else{
			throw new \Exception();
		}
	}
	
	private static function loadProfile( $uid ) {
		// charger l'utilisateur et ses droits
		$user=User::find($uid);
		// détruire la variable de session
		session_unset();
		// créer variable de session = profil chargé
		$_SESSION['droits']=$user->droits;
		$_SESSION['login']=$user->login;
		$_SESSION['IP']=$_SERVER['REMOTE_ADDR'];
	}
	
	public static function checkAccessRights ( $required ) {
		$acces=false;
		if(isset($_SESSION['droits'])&&$_SESSION['droits']>=$required){
			$acces=true;
		}
		return $acces;
	}
}