<?php
/**
* File: HttpRequest.php
* Creation Date: 14/11/2014
* description: Récupération requete courante
*
* @author: Corentin
**/

namespace picof\utils;

class HttpRequest {
	private $method, $script_name, $request_uri, $query, $get;

	public function __construct() {
		$this->method = $_SERVER['REQUEST_METHOD'];
		$this->script_name = $_SERVER['SCRIPT_NAME'];
		$this->request_uri = $_SERVER['REQUEST_URI'];
		$this->query = $_SERVER['QUERY_STRING'];
		$this->get = $_GET;
	}

	public function __get( $attname ) {
 		if (property_exists($this, $attname)) {
 			return $this->$attname ; 
 		} 
 		else throw new Exception("invalid property");
		}


	public function __set($attname, $attval) {
 		if (property_exists($this, $attname)) {
 			$this->$attname = $attval;
			 return $this->$attname; 
			}
		 else throw new Exception ("invalid property");
		}

	public function getPathInfo() {
		$info = str_replace(dirname($this->script_name), '', $this->request_uri);  //blogapp/cat?id=2
		$var = explode('?', $info);
		return $var[0];				//blogapp/cat
	}

}



?>