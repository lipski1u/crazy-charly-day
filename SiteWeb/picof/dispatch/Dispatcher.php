<?php

namespace picof\dispatch;
use picof\utils\HttpRequest as HttpRequest;


class Dispatcher{

protected $requete;
protected $routes;


public function __construct($req){
	$this->requete = $req;
	$this->routes = array();
}

public function addRoute($url, $controller, $methode){
	if (isset($url, $controller, $methode)){ //si les param ne sont pas nuls
		$couple = array("c" => $controller, "m" => $methode);
		$this->routes[$url] = $couple;
	}
}

function dispatch(){
	foreach($this->routes as $key => $value){
		if(strstr($this->requete->getPathInfo(),$key)){
			$bc = new $value['c']($this->requete);
			$bc->$value['m']();
			break;
		}
	}
}
			
}