<?php
require_once 'vendor/autoload.php';

use \picof\dispatch\Dispatcher;
use \picof\utils\HttpRequest;
use \conf\Connection;
use \app\model\Item;

Connection::connection('db.config.ini');

$req=new HttpRequest();
$app=new Dispatcher($req);

if($req->request_uri==='/www/lipski1u/CrazyCharlyDay/'){
	$req->request_uri='/www/lipski1u/CrazyCharlyDay/catalogue';
}


$app->addRoute('/acceuil','\app\control\WebController','acceuil');
$app->addRoute('/catalogue','\app\control\WebController','affListItem');
$app->addRoute('/item','\app\control\WebController','affItem');
$app->addRoute('/listepiece','\app\control\WebController','affListPiece');
$app->addRoute('/piece','\app\control\WebController','affListItemPiece');
$app->addRoute('/listetype','\app\control\WebController','affListType');
$app->addRoute('/type','\app\control\WebController','affListItemType');
$app->addRoute('/listecouleur','\app\control\WebController','affListCouleur');
$app->addRoute('/couleur','\app\control\WebController','affListItemCouleur');
$app->addRoute('/connexion','\app\control\WebController','connexion');
$app->addRoute('/deconnexion','\app\control\WebController','deconnexion');
$app->addRoute('/ajItem','\app\control\AdminController','ajoutItem');
$app->addRoute('/ajPiece','\app\control\AdminController','ajoutPiece');
$app->addRoute('/ajType','\app\control\AdminController','ajoutType');
$app->dispatch();