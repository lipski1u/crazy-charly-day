-- phpMyAdmin SQL Dump
-- version 4.1.14.7
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 05 Mars 2015 à 22:21
-- Version du serveur :  5.1.73
-- Version de PHP :  5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `chiesura1u`
--

-- --------------------------------------------------------

--
-- Structure de la table `ccd_items`
--

CREATE TABLE IF NOT EXISTS `ccd_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `piece_id` int(11) NOT NULL,
  `photo` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `couleur` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `prix` float NOT NULL,
  `type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Contenu de la table `ccd_items`
--

INSERT INTO `ccd_items` (`id`, `nom`, `description`, `piece_id`, `photo`, `couleur`, `prix`, `type_id`) VALUES
(1, 'Fauteuil gonflable ', 'Prenez-vous un peu de bon temps avec ce magnifique fauteuil gonflable. Utilisable dans un espace plutôt étroit ou bien dans une grande pièce. Il se mariera à merveille avec vos meubles qui se verront rajeunir ! ', 1, 'fauteuil_gonflable.jpg', 'Mauve', 150, 4),
(2, 'Sofa Gonflable', 'Seul ou à plusieurs, ce confortable sofa gonflable apportera une touche de gaieté dans votre appartement ou votre maison. Laissez place au nouveau, vos amis seront stupéfiés !', 1, 'sofa_gonflable.jpg', 'Orange', 175.99, 4),
(3, 'Fauteuil en carton', 'Non, le carton n''est pas fragile ! La preuve avec cet épatant fauteuil en carton. Grâce à une solide armature, ce fauteuil vous permettra de corriger votre maintient.', 1, 'fauteuil.jpg', 'Maron', 15.95, 1),
(4, 'Vache', 'Quoi de mieux qu''un petit peu de campagne dans un appartement ? Cette vache permet de ranger un tas d''objet tel que des livres, des magazines ou bien même un peu de paperasse.', 2, 'vache_blanche.jpg', 'Blanche', 9.99, 1),
(5, 'Fauteuil en palettes', 'Et oui, c''est bien des palettes de bois que vous voyez ! Ce fauteuil en palette représente l''originalité et l''art.', 2, 'meuble_palette.jpg', 'Marron', 230.9, 2),
(6, 'Tortue', 'Une magnifique tortue rouge pour habiller votre bureau. Fini les stylos qui trainent un peu partout.', 3, 'tortue_rouge.jpg', 'Rouge', 25.85, 1),
(7, 'Tortue', 'Une magnifique tortue brune pour habiller votre bureau. Fini les stylos qui trainent un peu partout.', 3, 'tortue_brun.jpg', 'Brun', 25.85, 1),
(8, 'Canard Gonflable', 'Ce gros canard gonflable revigore votre appartement. Petit retour en enfance !', 4, 'canard_gonflable.jpg', 'Jaune', 4.99, 4);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
