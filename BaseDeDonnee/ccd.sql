-- phpMyAdmin SQL Dump
-- version 4.1.14.7
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 05 Mars 2015 à 10:54
-- Version du serveur :  5.1.73
-- Version de PHP :  5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `lipski1u`
--

-- --------------------------------------------------------

--
-- Structure de la table `ccd_aimer`
--

CREATE TABLE IF NOT EXISTS `ccd_aimer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_item` int(11) NOT NULL,
  `id_internaute` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `ccd_commentaires`
--

CREATE TABLE IF NOT EXISTS `ccd_commentaires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_item` int(11) NOT NULL,
  `id_internaute` int(11) NOT NULL,
  `body` varchar(10000) NOT NULL,
  `note` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `ccd_internaute`
--

CREATE TABLE IF NOT EXISTS `ccd_internaute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `date_naiss` date NOT NULL,
  `date_inscrit` date NOT NULL,
  `genre` varchar(1) NOT NULL,
  `droits` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `ccd_items`
--

CREATE TABLE IF NOT EXISTS `ccd_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `piece_id` int(11) NOT NULL,
  `photo` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `couleur` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `prix` float NOT NULL,
  `type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Contenu de la table `ccd_items`
--

INSERT INTO `ccd_items` (`id`, `nom`, `description`, `piece_id`, `photo`, `couleur`, `prix`, `type_id`) VALUES
(1, 'Fauteuil gonflable ', '', 1, 'fauteuil_gonflable.jpg', 'Mauve', 150, 4),
(2, 'Sofa Gonflable', '', 1, 'sofa_gonflable.jpg', 'Orange', 175.99, 4),
(3, 'Fauteuil en carton', '', 1, 'fauteuil.jpg', 'Maron', 15.95, 1),
(4, 'Vache', '', 2, 'vache_blanche.jpg', 'Blanche', 9.99, 1),
(5, 'Fauteuil en palettes', '', 2, 'meuble_palette.jpg', 'Marron', 230.9, 2),
(6, 'Tortue', '', 3, 'tortue_rouge.jpg', 'Rouge', 25.85, 1),
(7, 'Tortue', '', 3, 'tortue_brun.jpg', 'Brun', 25.85, 1),
(8, 'Canard Gonflable', '', 4, 'canard_gonflable.jpg', 'Jaune', 4.99, 4);

-- --------------------------------------------------------

--
-- Structure de la table `ccd_pieces`
--

CREATE TABLE IF NOT EXISTS `ccd_pieces` (
  `id` int(11) NOT NULL,
  `nom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `ccd_pieces`
--

INSERT INTO `ccd_pieces` (`id`, `nom`, `description`) VALUES
(1, 'Salon', 'Là où on se repose'),
(2, 'Salle à manger', 'Là où on mange'),
(3, 'Chambre à coucher', 'Là où on fait dodo'),
(4, 'Salle de bain', 'J''en ressort tout propre');

-- --------------------------------------------------------

--
-- Structure de la table `ccd_selection_admin`
--

CREATE TABLE IF NOT EXISTS `ccd_selection_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_item` int(11) NOT NULL,
  `body` varchar(10000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `ccd_types`
--

CREATE TABLE IF NOT EXISTS `ccd_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `ccd_types`
--

INSERT INTO `ccd_types` (`id`, `type`) VALUES
(1, 'Carton'),
(2, 'Récupération'),
(3, 'Recyclage'),
(4, 'Gonflable');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
